using DG.Tweening.Core.Easing;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;

public class CoinPickup : MonoBehaviour
{  
    public int coinValue;
    public AudioClip coinSound;

    void Start()
    {       
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            GameController.Instance.CollectCoin(coinValue);
            PlayCoinSound();
            Destroy(gameObject);
        }
    }

    private void PlayCoinSound()
    {
        Debug.Log("PlaySound");
        SoundManager.Instance.PlaySFX(coinSound);

    }


}

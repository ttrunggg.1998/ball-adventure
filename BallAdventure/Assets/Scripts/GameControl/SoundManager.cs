using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : MonoBehaviour
{   
    public static SoundManager Instance;
    
    public AudioSource musicSource;
    public AudioSource sfxSource;
    private bool isMusicOn = true;
    //private bool isSFXOn = true;

    private void Awake()
    {
        isMusicOn = true;
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    public void ToggleMusic()
    {
        isMusicOn = !isMusicOn;

        if (isMusicOn)
        {
            musicSource.Play();
        }
        else
        {
            musicSource.Pause();
        }
    }
    //public void ToggleSFX()
    //{
    //    isSFXOn = !isSFXOn;

    //    if (isSFXOn)
    //    {
    //        sfxSource.Play();
    //    }
    //    else
    //    {
    //        sfxSource.Pause();
    //    }
    //}
    public void PlayMusic(AudioClip musicClip)
    {
        musicSource.clip = musicClip;
        musicSource.Play();
    }

    public void PlaySFX(AudioClip sfxClip)
    {
        sfxSource.PlayOneShot(sfxClip);
    }

}

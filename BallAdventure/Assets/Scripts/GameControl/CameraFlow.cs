using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraFlow : MonoBehaviour
{
    private Vector3 offset = new Vector3(0f, 0f, -10f);
    private float smoothTime = 0.25f;
    private Vector3 velocity = Vector3.zero;
    
    [SerializeField] private Transform target;
    [SerializeField] private float minY, maxY, minX, maxX;
    private void Update()
    {
        if (target == null) return;
        offset.y = minY;
       Vector3 targetPosition = target.position+offset;
        if (targetPosition.x < minX)
        {
            targetPosition.x = minX;
        }
        if (targetPosition.x > maxX)
        {
            targetPosition.x = maxX;
        }
        if (targetPosition.y > maxY)
        {
            targetPosition.y = maxY;
        }
        transform.position = Vector3.SmoothDamp(transform.position, targetPosition, ref velocity, smoothTime);
    }
    
}

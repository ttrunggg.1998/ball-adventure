using DG.Tweening.Core.Easing;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using Unity.VisualScripting;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameController : MonoBehaviour
{
    public AudioClip backgroundMusic;
    public static GameController Instance;
    public TextMeshProUGUI TotalCoin;
    public TextMeshProUGUI Collect;
    public int totalCoin;
    public int collectCoin;
    public GameObject popupWin;
    public GameObject popupLose;
    public CoinData coinData;
    //public LevelData levelData;



    private void Awake()
    {
        
        SoundManager.Instance.PlayMusic(backgroundMusic);
        if (Instance == null)
        {
            Instance = this;
        }
        else if (Instance != this)
        {
            Destroy(gameObject);
        }

        //DontDestroyOnLoad(gameObject);
    }
    private void Update()
    {
        totalCoin = coinData.currentCoins;
    }
    public void CollectCoin(int value)
    {
        collectCoin += value;
        coinData.currentCoins += value; 
        Collect.text = "Coin: " + collectCoin.ToString();      
    }

    public int level;
    [System.Obsolete]
    public void HomeScr()
    {   
        Application.LoadLevel("Home");
    }
    public void PlayNextLevel()
    {
        Debug.Log("Load level" + level);
        SceneManager.LoadScene("Tutorial");
    }
    public void ReplayLevel()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
    }
    public void PauseLevel() 
    {
        Time.timeScale = 0;
    }
    public void ResumeLevel()
    {
        Time.timeScale = 1;
    }

}

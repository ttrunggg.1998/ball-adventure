using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MySingleton : MonoBehaviour
{
    private static MySingleton instance;


    public static MySingleton Instance
    {
        get
        {
            if (instance == null)
            {
                instance = FindObjectOfType<MySingleton>();

                if (instance == null)
                {
                    GameObject singletonObject = new GameObject();
                    instance = singletonObject.AddComponent<MySingleton>();
                    singletonObject.name = typeof(MySingleton).ToString();
                }

                DontDestroyOnLoad(instance.gameObject);
            }

            return instance;
        }
    }

    public int myVariable;

    private void Awake()
    {

        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(this.gameObject);
        }
        else
        {
            Destroy(this.gameObject);
        }
    }
}

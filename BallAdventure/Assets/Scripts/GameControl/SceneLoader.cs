using System.Collections;
using System;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string sceneName;
    


    public void StartGame()
    {
        LoadingScene.Instance.LoadScene(sceneName);
        GameController.Instance.ResumeLevel();

    }
    //public void LoadScene()
    //{
    //    StartGame();
    //    SceneManager.LoadScene(sceneName);
    //    GameController.Instance.ResumeLevel();
    //}
   
}

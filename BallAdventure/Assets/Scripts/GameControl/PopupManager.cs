using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PopupManager : MonoBehaviour
{
    
    public static PopupManager Instance;
    public void ShowPopupWithDelay(float delayInSeconds)
    {
        StartCoroutine(ShowPopupAfterDelay(delayInSeconds));
    }

    private IEnumerator ShowPopupAfterDelay(float delayInSeconds)
    {
        yield return new WaitForSeconds(delayInSeconds);

    }
}

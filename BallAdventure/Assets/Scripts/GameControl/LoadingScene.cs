using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class LoadingScene : MonoBehaviour
{
    public static LoadingScene Instance;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }
    public void LoadScene(string sceneName)
    {
        //Debug.Log("Loadng");
        StartCoroutine(LoadAsync(sceneName));
    }
    private IEnumerator LoadAsync(string sceneName)
    {
        SceneManager.LoadScene("LoadingScene");
        yield return new WaitForSeconds(2.0f);
        AsyncOperation operation = SceneManager.LoadSceneAsync(sceneName);
        while (!operation.isDone)
        {           
            float progress = Mathf.Clamp01(operation.progress / .9f);
            //Debug.Log("Loading progress: " + (progress * 100) + "%");
           
            yield return null;
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Musicbutton : MonoBehaviour
{
    public SoundManager soundManager;
    public Toggle buttonMusic;
    private void Start()
    {
         buttonMusic = GetComponent<Toggle>();
       // Button buttonSFX = GetComponent<Button>();
       
        //buttonSFX.onClick.AddListener(ToggleSFX);
    }

    private void ToggleMusic()
    {
        soundManager.ToggleMusic();
    }
    //private void ToggleSFX()
    //{
    //    soundManager.ToggleSFX();
    //}

}

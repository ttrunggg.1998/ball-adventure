using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class PlayerControl : MonoBehaviour
{
    Vector3 move;

    private Rigidbody2D rb;
    public FixedJoystick Joystick;
    public Button BtnJump;


    public bool isGrounded;
    public float Movespeed, JumpForce;

    public AudioClip jumpSound;
    public AudioClip dieSound;
    public Animator SadAnim;
    public Animator playerAnimator;
    public Magnet MagnetIngame;

    void Start()
    {
        
        rb = GetComponent<Rigidbody2D>();
        BtnJump = GameObject.Find("JumpButton").GetComponent<Button>();
        BtnJump.onClick.AddListener(Jump);
    }
 
    private void Update()
    {
        float playerVelocity = GetComponent<Rigidbody2D>().velocity.magnitude;

        if (playerVelocity < 0.1f)
        {
            StartCoroutine(PlayIdleAnimation());
        }
        //if (Input.GetKey(KeyCode.A))
        //{
        //    rb.AddForce(Vector3.left * Movespeed);
        //}
        //if (Input.GetKey(KeyCode.D))
        //{
        //    rb.AddForce(Vector3.right * Movespeed);
        //}
        //if (Input.GetKeyUp(KeyCode.Space)&&isGrounded)
        //{
        //    rb.AddForce(Vector3.up * JumpForce);
        //    isGrounded = false;
        //}
    }
    private void FixedUpdate()
    {
        move.x = Joystick.Horizontal;
        Move();
    }

    private void Jump()
    { 
        if (isGrounded)
        {           
            rb.AddForce(Vector3.up * JumpForce);
            //PlaySmokeEffect();
            PlayJumpSound();
            isGrounded = false;
        }    
    }
    private void Move()
    {
        rb.AddForce(move * Movespeed);       
    }

    void OnCollisionEnter2D(Collision2D other)
    {
        if (other.gameObject.tag == "Ground")
        {
            isGrounded = true;
        }

        if (other.gameObject.CompareTag("Enemy"))
        {
            if (other.transform.position.y <= transform.position.y)
            {
                Destroy(other.gameObject);
                rb.AddForce(Vector3.up * JumpForce);
            }
            else
            {
                PlayDieSound();
                StartCoroutine(ShowPopupAfterDelay());
                SadAnim.SetTrigger("DieTrigger");
                //Destroy(gameObject);
                //Time.timeScale = 0;
                Debug.Log("GameOver");
            }
        }
        if (other.gameObject.tag == "Finish")
        {
            Time.timeScale = 0;
            GameController.Instance.popupWin.SetActive(true);
            GameController.Instance.TotalCoin.text = "Coin: " + GameController.Instance.totalCoin.ToString();
            GameController.Instance.collectCoin = 0;
        }
        if (other.gameObject.tag == "Die")
        {
            PlayDieSound();     
            StartCoroutine(ShowPopupAfterDelay());
            SadAnim.SetTrigger("DieTrigger");
            GameController.Instance.TotalCoin.text = "Coin: " + GameController.Instance.totalCoin.ToString();
            GameController.Instance.collectCoin = 0;
        }
    }

    IEnumerator ShowPopupAfterDelay()
    {
        yield return new WaitForSeconds(2);        
        GameController.Instance.popupLose.SetActive(true);        
    }
    private void PlayJumpSound()
    {
        Debug.Log("Jump");
        SoundManager.Instance.PlaySFX(jumpSound);
    }
    private void PlayDieSound()
    {
        Debug.Log("DieSFX");
        SoundManager.Instance.PlaySFX(dieSound);
    }
    private IEnumerator PlayIdleAnimation()
    {
        yield return new WaitForSeconds(3);
        playerAnimator.SetTrigger("IdleTrigger");
    }
    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.tag == "Magnet")
        {
            MagnetIngame.MagnetBooster = true;
            Destroy(other.gameObject);
        }
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BossController : MonoBehaviour
{
    public bool FindObject = false;
    private Transform player;
    private Rigidbody2D rb;
    public float moveSpeed = 1.0f;
    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        FindPlayer();
    }
    void FindPlayer()
    {
        if (FindObject==true) 
        {           
        Vector2 moveDirec = (player.position - transform.position).normalized;
        transform.Translate(moveDirec * moveSpeed * Time.deltaTime);
         }
    }
}

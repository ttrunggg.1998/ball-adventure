using System.Collections;
using System.Collections.Generic;
using System.Security.Cryptography;
using Unity.VisualScripting;
using UnityEngine;

public class EnemyController : MonoBehaviour
{
    public float moveSpeed = 1.0f; 
    private Transform player;
    public bool FindObject = false;
    public bool moveRight = false;
    private Rigidbody2D rb;
    public float RollSpeed;
    public float RollSpeed2;

    // Start is called before the first frame update
    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").transform;
        rb = GetComponent<Rigidbody2D>();
        
    }

    // Update is called once per frame
    void Update()
    {
        float rotation = RollSpeed * Time.deltaTime;
        float rotation2 = RollSpeed2 * Time.deltaTime;
        //FindPlayer();
        if (moveRight)
        {
            rb.AddTorque(rotation2);
            rb.velocity = new Vector2(moveSpeed, rb.velocity.y);

        }
        if (!moveRight)
        {
            rb.AddTorque(rotation);
            rb.velocity = new Vector2(-moveSpeed, rb.velocity.y);

        }
    }
    void FindPlayer()
    {
        //if (FindObject==true) 
        //{           
            Vector2 moveDirec = (player.position - transform.position).normalized;
            transform.Translate(moveDirec * moveSpeed * Time.deltaTime);
       // }
    }
    private void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.gameObject.CompareTag("Wall"))
        {
            moveRight = !moveRight;
        }

    }


}

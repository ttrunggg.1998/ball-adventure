using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RotationWoodCircle : MonoBehaviour
{
    Vector3 move;
    private Rigidbody2D rb;
    public GameObject WoodCircle;
    public float rotation = 10;
    // Start is called before the first frame update
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.AddTorque(rotation*Time.deltaTime);
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Magnet : MonoBehaviour
{  
    public bool MagnetBooster;
    public float magnetForce = 10f; 
    public LayerMask coinLayer; 
    public float magnetRadius = 5f; 
    private Rigidbody2D rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody2D>();
    }

    private void FixedUpdate()
    {
        if (MagnetBooster == true)
        {
            Collider2D[] coins = Physics2D.OverlapCircleAll(transform.position, magnetRadius, coinLayer);

            foreach (Collider2D coin in coins)
            {
                Vector2 direction = transform.position - coin.transform.position;
                coin.attachedRigidbody.AddForce(direction.normalized * magnetForce);
            }
        }     
    }

    private void OnDrawGizmosSelected()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireSphere(transform.position, magnetRadius);
    }
}

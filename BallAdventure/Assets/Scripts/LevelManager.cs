using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelManager : MonoBehaviour
{
    public int currentLevel = 1;
    public int maxUnlockedLevel = 5; 

    // Start is called before the first frame update
    void Start()
    {
        LoadLevelProgress();

    }

    public void CompleteLevel()
    {
        if (currentLevel >= maxUnlockedLevel)
        {
            maxUnlockedLevel = currentLevel + 1;
            SaveLevelProgress();
        }
    }

    private void SaveLevelProgress()
    {
        PlayerPrefs.SetInt("MaxUnlockedLevel", maxUnlockedLevel);
        PlayerPrefs.Save();
    }

    private void LoadLevelProgress()
    {
        maxUnlockedLevel = PlayerPrefs.GetInt("MaxUnlockedLevel", 1);
    }
}

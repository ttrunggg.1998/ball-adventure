using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "LevelData", menuName = "GameData/Level", order = 1)]
public class LevelData : ScriptableObject
{
    public int CurentLevel;
}

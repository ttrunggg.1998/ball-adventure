using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "CoinData", menuName = "GameData/CoinData", order = 1)]
public class CoinData : ScriptableObject
{
    public int currentCoins;
}
